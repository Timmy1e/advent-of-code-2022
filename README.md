# Advent of Code 2021

My solutions to this year's calendar from [adventofcode.com](https://adventofcode.com/2022).

I'll be using the [`cargo-aoc`](https://github.com/gobanos/cargo-aoc) for it's benchmarking and fetching of input files.
Running on my M1 Max 16" 2021 MacBook Pro.

## Day 1: Calorie Counting
Parsing was more difficult that the parts.
### Part 1
Found `67450` in `591.45 ns`.
Big found `4368180` in `12.414 ms`.
### Part 2
Found `199357` in `3.9599 µs`.
Big found `12967759` in `73.574 ms`.

## Day 2: Rock Paper Scissors
Some mapping. Solved it using enums.
### Part 1
Found `11603` in `3.4111 µs`.
Big found `116030000` in `35.313 ms`.
### Part 2
Found `12725` in `2.8997 µs`.
Big found `127250000` in `29.630 ms`.

## Day 3: Rucksack Reorganization
Split and chunk.
### Part 1
Found `7597` in `15.426 µs`.
Big found `792607` in `988.42 ms`.
### Part 2
Found `2607` in `19.002 µs`.
Big found `263598` in `2.1184 s`.

## Day 4: Camp Cleanup
Intersections and folding.
### Part 1
Found `657` in `799.76 ns`.
Big found `3466900` in `30.099 ms`.
### Part 2
Found `938` in `960.57 ns`.
Big found `6734252` in `70.373 ms`.

## Day 5: Supply Stacks
A fun day, parsing was more complex than the rest.
### Part 1
Found `QGTHFZBHV` in `6.1048 µs`.
Big found `QXWMZOHQIIZEXDCDVRDNYITZTKISAVCDLWNKVBQNGFDXXZKZRUOQAMKJOFOPFFTWQIIVMFOOSGTCLHPXNVRRUIBBPSHGFGULNFAUDSAVQDOMYTPVITJAPYJHZLXGEXCQUGXAPFUCPOZJUDLJSWEJPHWCDWKARGOPMKZRHVDUTHVAVXNUWOODGHEXBIXORGRWPTOPQHEW` in `799.57 ms`.
### Part 2
Found `MGDMPSZTM` in `17.129 µs`.
Big found `MPRUXFCQFSPJULHGIRCZXCLTVKNUSSCDVWTWOUHSIEBAXFCRMUVZAMBDGLMPCAUXQAIVOXFCSPBTRIPBNKAUKIKBAVNKWKBBDDSIAQNXQJQTKLSNQXMJYIJXAHBEGSJWIAFADPGBECLDRJVRZCVKGHWVZMBAOGGGHAARNZOWPISKTVNUMKACYHXXACEMTGBTTWYPUWSD` in `750.62 ms`.

## Day 6: Tuning Trouble
This was easy, just use `windows()` and `all_unique()`.
### Part 1
Found `1531` in `89.320 µs`.
Big found `85760445` in `11.458 s`.
### Part 2
Found `2518` in `221.44 µs`.
Big found `91845017` in `12.616 s`.

## Day 7: No Space Left On Device
Complete opposite of yesterday, this was a big pain.
### Part 1
Found `1423358` in `38.015 µs`.
Big found `2414990429` in `31.338 ms`.
Mega found `6000047435` in `984.03 ms`.
### Part 2
Found `545729` in `42.940 µs`.
Big found `2468263200` in `33.145 ms`.
Mega found `1235533557265` in `1.0030 s`.

## Day 8: Treetop Tree House
Recursion nightmare.
### Part 1
Found `1816` in `226.11 µs`.
Big found `252139` in `934.46 ms`.
Mega found `579961` in `101.10 s`.
### Part 2
Found `383520` in `236.91 µs`.
Big found `82629770164920` in `5.0549 s`.
Mega found `1874276660911504` in `28.205 s`.

## Day 9: Rope Bridge
Reading is difficult.
### Part 1
Found `6522` in `357.34 µs`.
Big found `8129855` in `1.9966 s`.
Mega found `81018461` in `22.583 s`.
### Part 2
Found `2717` in `377.69 µs`.
Big found `7750850` in `2.4094 s`.
Mega found `77264858` in `23.794 s`.

## Day 10: Cathode-Ray Tube
It's `isize` time.
### Part 1
Found `13720` in `194.48 ns`.
Big found `113260` in `407.48 µs`.
### Part 2
Found `FBURHZCH` in `551.58 ns`.
Big found `a picture of miku` in `1.2969 ms`.

## Day 11: Monkey in the Middle
It's *math* time.
### Part 1
Found `54752` in `30.776 µs`.
### Part 2
Found `13606755504` in `15.248 ms`.

## Day 12: Hill Climbing Algorithm
It's Dijkstra day! Although you don't need Dijkstra, a BFS would work as well.
### Part 1
Found `423` in `3.6079 ms`.
Big found `2616490` in `31.384 s`.
### Part 2
Found `416` in `2.8355 ms`.
Big found `2507471` in `41.627 s`.

## Day 13: Distress Signal
Reading is difficult, but it was implementing `Ord`.
### Part 1
Found `5340` in `2.2446 µs`.
Big found `56545` in `11.907 µs`.
### Part 2
Found `21276` in `71.232 µs`.
Big found `135072` in `364.44 µs`.

## Day 14: Regolith Reservoir
Very slow today.
### Part 1
Found `799` in `227.44 ms`.
### Part 2
Found `29076` in `50.294 s`.

## Day 15: Beacon Exclusion Zone
I was going to bruteforce it, but it was too slow.
### Part 1
Found `4797230` in `17.392 ms`.
### Part 2
Found `10291582906626` in `96.630 ms`.

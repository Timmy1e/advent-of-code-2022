//! # Day 1: Calorie Counting
//! https://adventofcode.com/2022/day/1

use aoc_runner_derive::{aoc, aoc_generator};

use itertools::Itertools;

#[aoc_generator(day1)]
pub fn prepare_input(input: &str) -> Vec<Vec<usize>> {
    let mut result = Vec::new();
    let mut current = Vec::new();

    for line in input.lines() {
        if line.is_empty() {
            result.push(current);
            current = Vec::new();
            continue;
        }
        current.push(line.parse().unwrap());
    }

    result
}

/// ## Find the Elf carrying the most Calories. How many total Calories is that Elf carrying?
#[aoc(day1, part1)]
pub fn solve_part1(input: &[Vec<usize>]) -> usize {
    input.iter().map(|calorie| calorie.iter().sum()).max().unwrap()
}

/// ## Find the top three Elves carrying the most Calories. How many Calories are those Elves carrying in total?
#[aoc(day1, part2)]
pub fn solve_part2(input: &[Vec<usize>]) -> usize {
    input.iter().map(|calorie| calorie.iter().sum()).sorted_by(|a: &usize, b: &usize| b.cmp(a)).as_slice()[0..=2].iter().sum()
}

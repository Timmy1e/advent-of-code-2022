//! # Day 10: Cathode-Ray Tube
//! https://adventofcode.com/2022/day/10

use aoc_runner_derive::{aoc, aoc_generator};

#[aoc_generator(day10)]
pub fn prepare_input(input: &str) -> Vec<Option<isize>> {
    input
        .lines()
        .map(|line| {
            if let Some((_, right)) = line.split_once(' ') {
                Some(right.parse().unwrap())
            } else {
                None
            }
        })
        .collect()
}

const CYCLES: [isize; 6] = [20, 60, 100, 140, 180, 220];

/// ## What is the sum of these six signal strengths?
#[aoc(day10, part1)]
pub fn solve_part1(input: &[Option<isize>]) -> isize {
    let mut cycle = 0_isize;
    let mut register = 1_isize;
    let mut result = 0_isize;

    for instruction in input {
        cycle += 1;
        if CYCLES.contains(&cycle) {
            result += cycle * register;
        }
        if let Some(a) = instruction {
            cycle += 1;
            if CYCLES.contains(&cycle) {
                result += cycle * register;
            }
            register += a;
        }
    }

    result
}
// Regular: `40`, Big: `1_024`
const SCREEN_WIDTH: usize = 1_024;
// Regular: `9`, Big: `512`
const SCREEN_HEIGHT: usize = 512;

/// ## What eight capital letters appear on your CRT?
#[aoc(day10, part2)]
pub fn solve_part2(input: &[Option<isize>]) -> String {
    let mut cycle = 0_isize;
    let mut register = 1_isize;
    let mut screen = String::with_capacity(SCREEN_WIDTH * SCREEN_HEIGHT);
    screen.push('\n');

    for instruction in input {
        render(&mut screen, &cycle, &register);
        cycle += 1;
        if let Some(a) = instruction {
            render(&mut screen, &cycle, &register);
            cycle += 1;
            register += a;
        }
    }

    screen
}

fn render(screen: &mut String, cycle: &isize, register: &isize) {
    if (cycle % SCREEN_WIDTH as isize).abs_diff(*register) < 2 {
        screen.push('█');
    } else {
        screen.push(' ');
    }
    if (cycle + 1) % (SCREEN_WIDTH as isize) == 0 {
        screen.push('\n');
    }
}

//! # Day 11: Monkey in the Middle
//! https://adventofcode.com/2022/day/11

use std::collections::HashMap;

use aoc_runner_derive::{aoc, aoc_generator};

use itertools::Itertools;
use lazy_static::lazy_static;
use regex::Regex;

lazy_static! {
    static ref REGEX_MONKEY: Regex = Regex::new(r"Monkey (\d+):").unwrap();
    static ref REGEX_ITEMS: Regex = Regex::new(r"Starting items: ((?:\d+(?:, )?)+)").unwrap();
    static ref REGEX_OPERATION: Regex =
        Regex::new(r"Operation: new = old ([*+]) (\d+|old)").unwrap();
    static ref REGEX_TEST: Regex = Regex::new(r"Test: divisible by (\d+)").unwrap();
    static ref REGEX_ACTION: Regex =
        Regex::new(r"If (?:true|false): throw to monkey (\d+)").unwrap();
}

#[aoc_generator(day11)]
pub fn prepare_input(input: &str) -> HashMap<usize, Monkey> {
    let mut monkeys = HashMap::new();

    for block in input.trim().split("\n\n") {
        let mut lines = block.lines();
        let line = lines.next();
        if line.is_none() {
            break;
        }

        let id = REGEX_MONKEY
            .captures(line.unwrap())
            .unwrap()
            .get(1)
            .unwrap()
            .as_str()
            .parse()
            .unwrap();

        let items = REGEX_ITEMS
            .captures(lines.next().unwrap())
            .unwrap()
            .get(1)
            .unwrap()
            .as_str()
            .split(", ")
            .map(|s| s.parse().unwrap())
            .collect();

        let operation = {
            let caps = REGEX_OPERATION.captures(lines.next().unwrap()).unwrap();
            if caps.get(1).unwrap().as_str() == "+" {
                let right = caps.get(2).unwrap().as_str();
                if right == "old" {
                    Operation::Multiply(2)
                } else {
                    Operation::Add(right.parse().unwrap())
                }
            } else {
                let right = caps.get(2).unwrap().as_str();
                if right == "old" {
                    Operation::MultiplySelf
                } else {
                    Operation::Multiply(right.parse().unwrap())
                }
            }
        };

        let test = REGEX_TEST
            .captures(lines.next().unwrap())
            .unwrap()
            .get(1)
            .unwrap()
            .as_str()
            .parse()
            .unwrap();

        let action = (
            REGEX_ACTION
                .captures(lines.next().unwrap())
                .unwrap()
                .get(1)
                .unwrap()
                .as_str()
                .parse()
                .unwrap(),
            REGEX_ACTION
                .captures(lines.next().unwrap())
                .unwrap()
                .get(1)
                .unwrap()
                .as_str()
                .parse()
                .unwrap(),
        );

        monkeys.insert(
            id,
            Monkey {
                items,
                operation,
                test,
                target: action,
                items_inspected: 0,
            },
        );
    }

    monkeys
}

/// ## What is the level of monkey business after 20 rounds of stuff-slinging simian shenanigans?
#[aoc(day11, part1)]
pub fn solve_part1(input: &HashMap<usize, Monkey>) -> usize {
    solve(&mut input.clone(), 20, |worry| worry / 3)
}

/// ## Worry levels are no longer divided by three after each item is inspected; you'll need to find another way to keep your worry levels manageable. Starting again from the initial state in your puzzle input, what is the level of monkey business after 10000 rounds?
#[aoc(day11, part2)]
pub fn solve_part2(input: &HashMap<usize, Monkey>) -> usize {
    solve(&mut input.clone(), 10_000, |worry| worry)
}

fn solve(
    monkeys: &mut HashMap<usize, Monkey>,
    rounds: usize,
    worry_modifier: WorryModifier,
) -> usize {
    let sorted_keys: Vec<usize> = monkeys.keys().cloned().sorted().collect();
    let modulo = monkeys.values().map(|monkey| monkey.test).product();
    for _ in 1..=rounds {
        for id in &sorted_keys {
            monkeys
                .get_mut(id)
                .unwrap()
                .run(modulo, worry_modifier)
                .into_iter()
                .for_each(|(target, item)| monkeys.get_mut(&target).unwrap().items.push(item));
        }
    }

    let input = monkeys
        .values()
        .sorted_by_key(|monkey| monkey.items_inspected)
        .collect::<Vec<&Monkey>>();

    input.last().unwrap().items_inspected * input.get(input.len() - 2).unwrap().items_inspected
}

type WorryModifier = fn(usize) -> usize;

#[derive(Clone, Debug)]
enum Operation {
    Add(usize),
    Multiply(usize),
    MultiplySelf,
}

impl Operation {
    #[inline]
    fn run(&self, worry_level: usize) -> usize {
        match self {
            Operation::Add(num) => worry_level + num,
            Operation::Multiply(num) => worry_level * num,
            Operation::MultiplySelf => worry_level * worry_level,
        }
    }
}

#[derive(Clone, Debug)]
pub struct Monkey {
    items: Vec<usize>,
    operation: Operation,
    test: usize,
    target: (usize, usize),
    items_inspected: usize,
}

impl Monkey {
    fn run(&mut self, modulo: usize, worry_modifier: WorryModifier) -> Vec<(usize, usize)> {
        self.items_inspected += self.items.len();
        let result = self
            .items
            .iter()
            .map(|&item| {
                let item = self.operation.run(item);
                let item = worry_modifier(item);
                let item = item % modulo;
                let send_to = if item % self.test == 0 {
                    self.target.0
                } else {
                    self.target.1
                };

                (send_to, item)
            })
            .collect();
        self.items.clear();
        result
    }
}

//! # Day 12: Hill Climbing Algorithm
//! https://adventofcode.com/2022/day/12

use std::{
    cmp::Reverse,
    collections::{BTreeMap, BinaryHeap},
};

use aoc_runner_derive::{aoc, aoc_generator};

#[aoc_generator(day12)]
pub fn prepare_input(input: &str) -> Day12 {
    let mut source = None;
    let mut destination = None;

    Day12 {
        map: input
            .trim()
            .lines()
            .enumerate()
            .map(|(y, line)| {
                line.trim()
                    .chars()
                    .enumerate()
                    .map(|(x, ch)| match ch {
                        'S' => {
                            source = Some((x, y));
                            'a'.to_digit(36).unwrap()
                        }
                        'E' => {
                            destination = Some((x, y));
                            'z'.to_digit(36).unwrap()
                        }
                        _ => ch.to_digit(36).unwrap(),
                    } as usize)
                    .collect()
            })
            .collect(),
        source: source.unwrap(),
        destination: destination.unwrap(),
    }
}

/// ## What is the fewest steps required to move from your current position to the location that should get the best signal?
#[aoc(day12, part1)]
pub fn solve_part1(
    Day12 {
        map,
        source,
        destination,
    }: &Day12,
) -> usize {
    let mut graph = Graph::new();

    for (y, line) in map.iter().enumerate() {
        for (x, height) in line.iter().enumerate() {
            let mut sub_graph = BTreeMap::new();
            if x != 0 && map[y][x - 1] - 1 <= *height {
                sub_graph.insert((x - 1, y), 1);
            }
            if x != line.len() - 1 && map[y][x + 1] - 1 <= *height {
                sub_graph.insert((x + 1, y), 1);
            }
            if y != 0 && map[y - 1][x] - 1 <= *height {
                sub_graph.insert((x, y - 1), 1);
            }
            if y != map.len() - 1 && map[y + 1][x] - 1 <= *height {
                sub_graph.insert((x, y + 1), 1);
            }
            graph.insert((x, y), sub_graph);
        }
    }

    let res = dijkstra(&graph, source);

    res[destination].unwrap().1
}

/// ## What is the fewest steps required to move starting from any square with elevation `a` to the location that should get the best signal?
#[aoc(day12, part2)]
pub fn solve_part2(
    Day12 {
        map, destination, ..
    }: &Day12,
) -> usize {
    let mut graph = Graph::new();

    for (y, line) in map.iter().enumerate() {
        for (x, height) in line.iter().enumerate() {
            let mut sub_graph = BTreeMap::new();
            if x != 0 && map[y][x - 1] + 1 >= *height {
                sub_graph.insert((x - 1, y), 1);
            }
            if x != line.len() - 1 && map[y][x + 1] + 1 >= *height {
                sub_graph.insert((x + 1, y), 1);
            }
            if y != 0 && map[y - 1][x] + 1 >= *height {
                sub_graph.insert((x, y - 1), 1);
            }
            if y != map.len() - 1 && map[y + 1][x] + 1 >= *height {
                sub_graph.insert((x, y + 1), 1);
            }
            graph.insert((x, y), sub_graph);
        }
    }

    let res = dijkstra(&graph, destination);
    let lowest = 'a'.to_digit(36).unwrap() as usize;

    map.iter()
        .enumerate()
        .flat_map(|(y, line)| {
            line.iter().enumerate().filter_map(move |(x, height)| {
                if height == &lowest {
                    Some((x, y))
                } else {
                    None
                }
            })
        })
        .filter_map(|point| {
            if let Some(Some(thing)) = res.get(&point) {
                Some(thing.1)
            } else {
                None
            }
        })
        .min()
        .unwrap()
}

type Point = (usize, usize);
type Map = Vec<Vec<usize>>;
type Graph = BTreeMap<Point, BTreeMap<Point, usize>>;

pub struct Day12 {
    map: Map,
    source: Point,
    destination: Point,
}

#[inline]
/// Based on https://github.com/TheAlgorithms/Rust/blob/master/src/graph/dijkstra.rs.
fn dijkstra(graph: &Graph, source: &Point) -> BTreeMap<Point, Option<(Point, usize)>> {
    let mut result = BTreeMap::new();
    let mut priority = BinaryHeap::new();

    result.insert(*source, None);
    for (point, weight) in &graph[source] {
        result.insert(*point, Some((*source, *weight)));
        priority.push(Reverse((*weight, point, source)));
    }

    while let Some(Reverse((distance, this_point, previous_point))) = priority.pop() {
        match result[this_point] {
            Some((point, dist)) if point == *previous_point && dist == distance => {}
            _ => continue,
        }

        for (next, weight) in &graph[this_point] {
            match result.get(next) {
                Some(Some((_, dist_next))) if distance + *weight >= *dist_next => {}
                Some(None) => {}
                _ => {
                    result.insert(*next, Some((*this_point, *weight + distance)));
                    priority.push(Reverse((*weight + distance, next, this_point)));
                }
            }
        }
    }

    result
}

//! # Day 13: Distress Signal
//! https://adventofcode.com/2022/day/13

use std::cmp::Ordering;

use aoc_runner_derive::{aoc, aoc_generator};

use itertools::Itertools;
use lazy_static::lazy_static;
use serde_json::Value;

#[aoc_generator(day13)]
pub fn prepare_input(input: &str) -> Vec<(ListInt, ListInt)> {
    input
        .trim()
        .split("\n\n")
        .filter_map(|pair| {
            pair.split('\n')
                .map(serde_json::from_str::<Value>)
                .filter_map(Result::ok)
                .map(ListInt::from)
                .collect_tuple()
        })
        .collect()
}

/// ## Determine which pairs of packets are already in the right order. What is the sum of the indices of those pairs?
#[aoc(day13, part1)]
pub fn solve_part1(input: &[(ListInt, ListInt)]) -> usize {
    input
        .iter()
        .enumerate()
        .filter_map(|(index, (left, right))| {
            if left.cmp(right) == Ordering::Less {
                Some(index + 1)
            } else {
                None
            }
        })
        .sum()
}

lazy_static! {
    static ref DIVIDER_PACKET1: ListInt = ListInt::List(vec![ListInt::List(vec![ListInt::Int(2)])]);
    static ref DIVIDER_PACKET2: ListInt = ListInt::List(vec![ListInt::List(vec![ListInt::Int(6)])]);
}

/// ## Organize all of the packets into the correct order. What is the decoder key for the distress signal?
#[aoc(day13, part2)]
pub fn solve_part2(input: &[(ListInt, ListInt)]) -> usize {
    let mut list = input
        .iter()
        .flat_map(|(left, right)| [left, right])
        .collect::<Vec<&ListInt>>();
    list.extend_from_slice(&[&DIVIDER_PACKET1, &DIVIDER_PACKET2]);
    list.sort_unstable();

    (list
        .iter()
        .position(|&item| item == &DIVIDER_PACKET1 as &ListInt)
        .unwrap()
        + 1)
        * (list
            .iter()
            .position(|&item| item == &DIVIDER_PACKET2 as &ListInt)
            .unwrap()
            + 1)
}

#[derive(Clone, Eq, PartialEq)]
pub enum ListInt {
    List(Vec<ListInt>),
    Int(usize),
}

impl Ord for ListInt {
    fn cmp(&self, other: &Self) -> Ordering {
        match (self, other) {
            (Self::Int(left), Self::Int(right)) => left.cmp(right),
            (_, Self::Int(int)) => self.cmp(&Self::List(vec![Self::Int(*int)])),
            (Self::Int(int), _) => Self::List(vec![Self::Int(*int)]).cmp(other),
            (Self::List(left), Self::List(right)) => {
                for (left, right) in left.iter().zip(right) {
                    let res = left.cmp(right);
                    if res != Ordering::Equal {
                        return res;
                    }
                }
                left.len().cmp(&right.len())
            }
        }
    }
}

impl PartialOrd for ListInt {
    #[inline]
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl From<Value> for ListInt {
    fn from(value: Value) -> Self {
        match value {
            Value::Number(num) => ListInt::Int(num.as_u64().unwrap() as usize),
            Value::Array(vec) => ListInt::List(vec.into_iter().map(ListInt::from).collect()),
            _ => panic!("Unexpected: {:?}", value),
        }
    }
}

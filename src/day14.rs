//! # Day 14: Regolith Reservoir
//! https://adventofcode.com/2022/day/14

use aoc_runner_derive::{aoc, aoc_generator};

use geo::{coord, BoundingRect, Contains, Intersects, LineString, MultiPoint, Point};
use lazy_static::lazy_static;

lazy_static! {
    static ref UP: Point<isize> = Point::from(coord! {x: 0, y: -1});
    static ref DOWN: Point<isize> = Point::from(coord! {x: 0, y: 1});
    static ref LEFT: Point<isize> = Point::from(coord! {x: -1, y: 0});
    static ref RIGHT: Point<isize> = Point::from(coord! {x: 1, y: 0});
}

#[aoc_generator(day14)]
pub fn prepare_input(input: &str) -> Day14 {
    (
        input
            .trim()
            .lines()
            .map(|line| {
                LineString::new(
                    line.split(" -> ")
                        .map(|point| {
                            let point = point.split_once(',').unwrap();
                            (point.0.parse().unwrap(), point.1.parse().unwrap()).into()
                        })
                        .collect(),
                )
            })
            .collect(),
        (500, 0).into(),
    )
}

#[aoc(day14, part1)]
/// ## Using your scan, simulate the falling sand. How many units of sand come to rest before sand starts flowing into the abyss below?
pub fn solve_part1((rocks, source): &Day14) -> usize {
    let death_plane = rocks
        .iter()
        .map(|rocks| rocks.bounding_rect().unwrap().max().y)
        .max()
        .unwrap();

    physics(rocks, source, death_plane)
}

#[aoc(day14, part2)]
/// ## Using your scan, simulate the falling sand until the source of the sand becomes blocked. How many units of sand come to rest?
pub fn solve_part2((rocks, source): &Day14) -> usize {
    let death_plane = rocks
        .iter()
        .map(|rocks| rocks.bounding_rect().unwrap().max().y)
        .max()
        .unwrap()
        + 2;

    let mut rocks = rocks.clone();
    rocks.push(LineString::new(vec![
        coord! {x: isize::MIN, y: death_plane},
        coord! {x: isize::MAX, y: death_plane},
    ]));

    physics(&rocks, source, death_plane)
}

type Day14 = (Vec<LineString<isize>>, Point<isize>);

#[inline]
fn physics(rocks: &[LineString<isize>], source: &Point<isize>, death_plane: isize) -> usize {
    let up = *UP;
    let down = *DOWN;
    let left = *LEFT;
    let right = *RIGHT;
    let mut sands = MultiPoint::<isize>::new(vec![]);

    'outer: loop {
        let mut sand = *source;

        loop {
            if sand.y() >= death_plane {
                break 'outer;
            }

            sand += down;

            let hits = rocks.iter().any(|rock| sand.intersects(rock)) || sands.contains(&sand);

            if !hits {
                continue;
            }

            let left_sand = sand + left;
            let right_sand = sand + right;
            if !rocks.iter().any(|rock| left_sand.intersects(rock)) && !sands.contains(&left_sand) {
                sand += left;
            } else if !rocks.iter().any(|rock| right_sand.intersects(rock))
                && !sands.contains(&right_sand)
            {
                sand += right;
            } else {
                sand += up;
                sands.0.push(sand);

                if &sand == source {
                    break 'outer;
                }

                break;
            }
        }
    }

    sands.0.len()
}

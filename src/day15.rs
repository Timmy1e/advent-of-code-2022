//! # Day 15: Beacon Exclusion Zone
//! https://adventofcode.com/2022/day/15

use aoc_runner_derive::{aoc, aoc_generator};

use geo::Point;
use lazy_static::lazy_static;
use regex::Regex;

lazy_static! {
    static ref REGEX_COORDINATE: Regex = Regex::new(r"x=(-?\d+), y=(-?\d+)").unwrap();
}

#[aoc_generator(day15)]
pub fn prepare_input(input: &str) -> Vec<SensorBeacon> {
    input
        .trim()
        .lines()
        .map(|line| {
            let (sensor, beacon) = line.split_once(':').unwrap();
            let sensor = REGEX_COORDINATE.captures(sensor).unwrap();
            let beacon = REGEX_COORDINATE.captures(beacon).unwrap();
            SensorBeacon::new(
                Point::new(
                    sensor.get(1).unwrap().as_str().parse().unwrap(),
                    sensor.get(2).unwrap().as_str().parse().unwrap(),
                ),
                Point::new(
                    beacon.get(1).unwrap().as_str().parse().unwrap(),
                    beacon.get(2).unwrap().as_str().parse().unwrap(),
                ),
            )
        })
        .collect()
}

const PART1: isize = 10; //2_000_000;

#[aoc(day15, part1)]
/// ## Consult the report from the sensors you just deployed. In the row where y=2000000, how many positions cannot contain a beacon?
pub fn solve_part1(sensor_beacons: &[SensorBeacon]) -> usize {
    let sensor_beacons = sensor_beacons
        .iter()
        .filter(|sb| sb.can_reach_y(&PART1))
        .collect::<Vec<&SensorBeacon>>();

    let leftest = sensor_beacons
        .iter()
        .map(|sb| sb.sensor.x() - sb.radius)
        .min()
        .unwrap();
    let rightest = sensor_beacons
        .iter()
        .map(|sb| sb.sensor.x() + sb.radius)
        .max()
        .unwrap();

    let mut blocked = 0;

    for x in leftest..=rightest {
        if sensor_beacons.iter().any(|&sb| {
            !(sb.beacon.x() == x && sb.beacon.y() == PART1)
                && sb.radius >= sb.distance_to_sensor(&x, &PART1)
        }) {
            blocked += 1;
        }
    }

    blocked
}

const PART2_MIN: isize = 0;
const PART2_MAX: isize = 4_000_000;

#[aoc(day15, part2)]
/// ## Find the only possible position for the distress beacon. What is its tuning frequency?
pub fn solve_part2(sensor_beacons: &[SensorBeacon]) -> usize {
    for sensor_beacon in sensor_beacons {
        let left = sensor_beacon.sensor.x() - sensor_beacon.radius - 1;
        let right = sensor_beacon.sensor.x() + sensor_beacon.radius + 1;
        for x in left..=right {
            if x < PART2_MIN {
                continue;
            } else if x > PART2_MAX {
                break;
            }

            // Check the left and right tips
            if (x == left || x == right)
                && check_beacons(sensor_beacons, &x, &sensor_beacon.sensor.y())
            {
                return ((x * PART2_MAX) + sensor_beacon.sensor.y()) as usize;
            }

            // Get the Y radius for the X, because we are working with a "circle"
            let remaining_y = sensor_beacon.radius - (sensor_beacon.sensor.x() - x).abs();
            let top = sensor_beacon.sensor.y() - remaining_y - 1;
            if top >= PART2_MIN && check_beacons(sensor_beacons, &x, &top) {
                return ((x * PART2_MAX) + top) as usize;
            }

            let bottom = sensor_beacon.sensor.y() + remaining_y + 1;
            if bottom <= PART2_MAX && check_beacons(sensor_beacons, &x, &bottom) {
                return ((x * PART2_MAX) + bottom) as usize;
            }
        }
    }
    unreachable!()
}

fn check_beacons(sensor_beacons: &[SensorBeacon], x: &isize, y: &isize) -> bool {
    sensor_beacons
        .iter()
        .all(|sb| sb.radius < sb.distance_to_sensor(x, y))
}

#[derive(Clone, Debug)]
pub struct SensorBeacon {
    sensor: Point<isize>,
    beacon: Point<isize>,
    radius: isize,
}

impl SensorBeacon {
    fn new(sensor: Point<isize>, beacon: Point<isize>) -> Self {
        Self {
            sensor,
            beacon,
            radius: (sensor.x() - beacon.x()).abs() + (sensor.y() - beacon.y()).abs(),
        }
    }

    fn distance_to_sensor(&self, x: &isize, y: &isize) -> isize {
        (self.sensor.x() - x).abs() + (self.sensor.y() - y).abs()
    }

    fn can_reach_y(&self, y: &isize) -> bool {
        self.radius >= (self.sensor.y() - y).abs()
    }
}

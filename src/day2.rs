//! # Day 2: Rock Paper Scissors
//! https://adventofcode.com/2022/day/2

use aoc_runner_derive::{aoc, aoc_generator};

use enum_repr::EnumRepr;

#[aoc_generator(day2, part1)]
pub fn prepare_part1(input: &str) -> Vec<BattleRound> {
    input.lines().map(|line| {
        let mut line = line.chars();
        BattleRound::new(line.next().unwrap(), line.nth(1).unwrap())
    }).collect()
}

#[aoc_generator(day2, part2)]
pub fn prepare_part2(input: &str) -> Vec<IndicationRound> {
    input.lines().map(|line| {
        let mut line = line.chars();
        IndicationRound::new(line.next().unwrap(), line.nth(1).unwrap())
    }).collect()
}

/// ## Find the Elf carrying the most Calories. How many total Calories is that Elf carrying?
#[aoc(day2, part1)]
pub fn solve_part1(input: &[BattleRound]) -> usize {
    input.iter().map(|battle| battle.battle()).sum()
}

/// ## Following the Elf's instructions for the second column, what would your total score be if everything goes exactly according to your strategy guide?
#[aoc(day2, part2)]
pub fn solve_part2(input: &[IndicationRound]) -> usize {
    input.iter().map(|battle| battle.indicate()).sum()
}

#[derive(Debug, Eq, PartialEq)]
#[EnumRepr(type = "usize")]
enum RockPaperScissors {
    Rock = 1,
    Paper = 2,
    Scissors = 3,
}

impl RockPaperScissors {
    fn new(input: char) -> Self {
        match input {
            'A' | 'X' => Self::Rock,
            'B' | 'Y' => Self::Paper,
            'C' | 'Z' => Self::Scissors,
            _ => panic!("Unknown hand: {:?}", input)
        }
    }
}

#[derive(Debug, Eq, PartialEq)]
enum Indication {
    Lose,
    Draw,
    Win,
}

impl Indication {
    fn new(input: char) -> Self {
        match input {
            'X' => Self::Lose,
            'Y' => Self::Draw,
            'Z' => Self::Win,
            _ => panic!("Unknown indication: {:?}", input)
        }
    }
}

#[derive(Debug)]
pub struct BattleRound {
    left: RockPaperScissors,
    right: RockPaperScissors,
}

impl BattleRound {
    fn new(left: char, right: char) -> Self {
        Self { left: RockPaperScissors::new(left), right: RockPaperScissors::new(right) }
    }

    fn battle(&self) -> usize {
        if self.left == self.right {
            return self.right.repr() + 3;
        }
        match self.left {
            RockPaperScissors::Rock if self.right == RockPaperScissors::Paper => self.right.repr() + 6,
            RockPaperScissors::Paper if self.right == RockPaperScissors::Scissors => self.right.repr() + 6,
            RockPaperScissors::Scissors if self.right == RockPaperScissors::Rock => self.right.repr() + 6,
            _ => self.right.repr(),
        }
    }
}

#[derive(Debug)]
pub struct IndicationRound {
    left: RockPaperScissors,
    right: Indication,
}

impl IndicationRound {
    fn new(left: char, right: char) -> Self {
        Self { left: RockPaperScissors::new(left), right: Indication::new(right) }
    }

    fn indicate(&self) -> usize {
        if self.right == Indication::Draw {
            return self.left.repr() + 3;
        }

        match self.left {
            RockPaperScissors::Rock => if self.right == Indication::Win {
                RockPaperScissors::Paper.repr() + 6
            } else {
                RockPaperScissors::Scissors.repr()
            },
            RockPaperScissors::Paper => if self.right == Indication::Win {
                RockPaperScissors::Scissors.repr() + 6
            } else {
                RockPaperScissors::Rock.repr()
            },
            RockPaperScissors::Scissors => if self.right == Indication::Win {
                RockPaperScissors::Rock.repr() + 6
            } else {
                RockPaperScissors::Paper.repr()
            }
        }
    }
}

//! # Day 3: Rucksack Reorganization
//! https://adventofcode.com/2022/day/3

use aoc_runner_derive::{aoc, aoc_generator};

#[aoc_generator(day3)]
pub fn prepare_input(input: &str) -> Vec<Vec<usize>> {
    input.lines().map(|line| {
        line.chars().map(|ch| {
            let (mut ch, mut result) = if ch.is_ascii_uppercase() {
                (ch.to_ascii_lowercase(), 27)
            } else {
                (ch, 1)
            };
            while ch != 'a' {
                ch = char::from_digit(ch.to_digit(36).unwrap() - 1, 36).unwrap();
                result += 1;
            }
            result
        }).collect::<Vec<usize>>()
    }).collect()
}

/// ## Find the item type that appears in both compartments of each rucksack. What is the sum of the priorities of those item types?
#[aoc(day3, part1)]
pub fn solve_part1(input: &[Vec<usize>]) -> usize {
    let mut result = 0;

    for bag in input {
        let (left, right) = bag.split_at(bag.len() / 2);
        for item in left {
            if right.contains(item) {
                result += item;
                break;
            }
        }
    }

    result
}

/// ## Find the item type that corresponds to the badges of each three-Elf group. What is the sum of the priorities of those item types?
#[aoc(day3, part2)]
pub fn solve_part2(input: &[Vec<usize>]) -> usize {
    let mut result = 0;

    for window in input.chunks(3) {
        for item in &window[0] {
            if window[1].contains(item) && window[2].contains(item) {
                result += item;
                break;
            }
        }
    }

    result
}

//! # Day 4: Camp Cleanup
//! https://adventofcode.com/2022/day/4

use aoc_runner_derive::{aoc, aoc_generator};

#[aoc_generator(day4)]
pub fn prepare_input(input: &str) -> Vec<Pair> {
    input.lines().map(|line| {
        let mut nums = line.split(&[',', '-']);
        ((nums.next().unwrap().parse().unwrap(), nums.next().unwrap().parse().unwrap()), (nums.next().unwrap().parse().unwrap(), nums.next().unwrap().parse().unwrap()))
    }).collect()
}

/// ## In how many assignment pairs does one range fully contain the other?
#[aoc(day4, part1)]
pub fn solve_part1(input: &[Pair]) -> usize {
    input.iter().fold(0, |accumulator, pair| {
        if (pair.0.0 >= pair.1.0 && pair.0.1 <= pair.1.1) || (pair.1.0 >= pair.0.0 && pair.1.1 <= pair.0.1) {
            accumulator + 1
        } else {
            accumulator
        }
    })
}

/// ## In how many assignment pairs do the ranges overlap?
#[aoc(day4, part2)]
pub fn solve_part2(input: &[Pair]) -> usize {
    input.iter().fold(0, |accumulator, pair| {
        if (pair.0.0 <= pair.1.0 && pair.0.1 >= pair.1.0) || (pair.0.0 <= pair.1.1 && pair.0.1 >= pair.1.1) || (pair.1.0 <= pair.0.0 && pair.1.1 >= pair.0.0) || (pair.1.0 <= pair.0.1 && pair.1.1 >= pair.0.1) {
            accumulator + 1
        } else {
            accumulator
        }
    })
}


type Range = (usize, usize);
pub type Pair = (Range, Range);

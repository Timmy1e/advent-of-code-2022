//! # Day 5: Supply Stacks
//! https://adventofcode.com/2022/day/5

use std::ops::IndexMut;

use aoc_runner_derive::{aoc, aoc_generator};

use itertools::Itertools;
use regex::Regex;

#[aoc_generator(day5)]
pub fn prepare_input(input: &str) -> Input {
    let mut lines = input.lines();

    let steps_regex = Regex::new(r"move | from | to ").unwrap();

    let mut crates = Crates::default();
    loop {
        let line = lines.next().unwrap();
        if line.trim().is_empty() {
            break; // Done with the crates
        }

        for (index, mut chunk) in line.chars().chunks(4).into_iter().enumerate() {
            let ch = chunk.nth(1).unwrap();
            if crates.len() <= index {
                crates.push(Vec::new())
            }
            if ch != ' ' {
                crates.index_mut(index).push(ch);
            }
        }
    }
    let crates = crates.into_iter().map(|mut c| {
        c.remove(c.len() - 1);
        c.reverse();
        c
    }).collect();

    let mut steps = Steps::default();
    for line in lines {
        if line.trim().is_empty() {
            break; // Done with the steps
        }

        let mut bits = steps_regex.split(line);
        bits.next();
        steps.push(Step {
            count: bits.next().unwrap().parse().unwrap(),
            from: bits.next().unwrap().parse::<usize>().unwrap() - 1,
            to: bits.next().unwrap().parse::<usize>().unwrap() - 1,
        })
    }

    (crates, steps)
}

/// ## After the rearrangement procedure completes, what crate ends up on top of each stack?
#[aoc(day5, part1)]
pub fn solve_part1((crates, steps): &Input) -> String {
    let mut crates = crates.clone();
    for step in steps {
        for _ in 1..=step.count {
            let tmp = crates.get_mut(step.from).unwrap().pop().unwrap();
            crates.get_mut(step.to).unwrap().push(tmp);
        }
    }

    let crates_len = crates.len();
    crates.into_iter().fold(String::with_capacity(crates_len), |mut accumulator, stack| {
        accumulator.push(*stack.last().unwrap());
        accumulator
    })
}

/// ## After the rearrangement procedure completes, what crate ends up on top of each stack?
#[aoc(day5, part2)]
pub fn solve_part2((crates, steps): &Input) -> String {
    let mut crates = crates.clone();
    for step in steps {
        let mut tmp = Vec::with_capacity(step.count);
        for _ in 1..=step.count {
            tmp.push(crates.get_mut(step.from).unwrap().pop().unwrap());
        }
        tmp.reverse();
        crates.get_mut(step.to).unwrap().append(&mut tmp);
    }

    let crates_len = crates.len();
    crates.into_iter().fold(String::with_capacity(crates_len), |mut accumulator, stack| {
        accumulator.push(*stack.last().unwrap());
        accumulator
    })
}

pub type Input = (Crates, Steps);
pub type Crates = Vec<Stack>;
pub type Stack = Vec<char>;
pub type Steps = Vec<Step>;

#[derive(Debug)]
pub struct Step {
    count: usize,
    from: usize,
    to: usize,
}

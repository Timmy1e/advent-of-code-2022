//! # Day 6: Tuning Trouble
//! https://adventofcode.com/2022/day/6

use aoc_runner_derive::{aoc, aoc_generator};

use itertools::Itertools;

#[aoc_generator(day6)]
pub fn prepare_input(input: &str) -> Vec<char> {
    input.trim().chars().collect()
}

/// ## How many characters need to be processed before the first start-of-packet marker is detected?
#[aoc(day6, part1)]
pub fn solve_part1(input: &[char]) -> usize {
    solve(input, 4)
}

/// ## How many characters need to be processed before the first start-of-message marker is detected?
#[aoc(day6, part2)]
pub fn solve_part2(input: &[char]) -> usize {
    solve(input, 14)
}

#[inline]
fn solve(input: &[char], size: usize) -> usize {
    let mut result = size;
    for window in input.windows(size) {
        if window.iter().all_unique() {
            break;
        }
        result += 1;
    }
    result
}

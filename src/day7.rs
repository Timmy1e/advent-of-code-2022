//! # Day 7: No Space Left On Device
//! https://adventofcode.com/2022/day/7

use std::{collections::{HashMap}, sync::Mutex, fmt::{Debug}};
use std::rc::Rc;

use aoc_runner_derive::{aoc, aoc_generator};

#[aoc_generator(day7)]
pub fn prepare_input(input: &str) -> Vec<TerminalLine> {
    input.lines().filter_map(|line| {
        let parts = line.trim().split_once(' ').unwrap();
        match parts.0 {
            "$" if parts.1 == "ls" => None,
            "$" => Some(TerminalLine::ChangeDir(parts.1.split_once(' ').unwrap().1.to_string())),
            "dir" => Some(TerminalLine::Dir(parts.1.to_string())),
            _ => Some(TerminalLine::File(parts.0.parse().unwrap())),
        }
    }).collect()
}

/// ## Find all of the directories with a total size of at most 100000. What is the sum of the total sizes of those directories?
#[aoc(day7, part1)]
pub fn solve_part1(input: &[TerminalLine]) -> usize {
    let root = build_file_system(input);
    let mut root = root.lock().unwrap();

    fn inner(dir: &mut Dir) -> usize {
        let own_size = dir.size();
        dir.dirs.iter().fold(
            if own_size < 100_000 { own_size } else { 0 },
            |acc, (_, dir)| acc + inner(&mut dir.lock().unwrap()),
        )
    }

    inner(&mut root)
}

const PART2_SIZE: usize = PART2_TOTAL_SIZE - PART2_NEEDED_SIZE;
// Regular: `70_000_000`, Big: `3_000_000_000`, Mega: `1_300_000_000_000`
const PART2_TOTAL_SIZE: usize = 70_000_000;
// Regular: `30_000_000`, Big: `700_000_000`, Mega: `300_000_000_000`
const PART2_NEEDED_SIZE: usize = 30_000_000;

/// ## Find the smallest directory that, if deleted, would free up enough space on the filesystem to run the update. What is the total size of that directory?
#[aoc(day7, part2)]
pub fn solve_part2(input: &[TerminalLine]) -> usize {
    let root = build_file_system(input);
    let mut root = root.lock().unwrap();
    let size_to_free = root.size() - PART2_SIZE;

    fn inner(size_to_free: usize, dir: &mut Dir) -> Vec<usize> {
        let mut result = Vec::with_capacity(dir.dirs.len() + 1);
        let own_size = dir.size();
        if own_size >= size_to_free {
            result.push(dir.size());
        }

        for dir in dir.dirs.values() {
            result.append(&mut inner(size_to_free, &mut dir.lock().unwrap()))
        }

        result
    }

    *inner(size_to_free, &mut root).iter().min().unwrap()
}


fn build_file_system(input: &[TerminalLine]) -> Rc<Mutex<Dir>> {
    let fs = Rc::new(Mutex::new(Dir::default()));

    let mut path = vec![fs.clone()];

    for line in input {
        match line {
            TerminalLine::ChangeDir(name) => match name.as_str() {
                "/" => path = vec![path.first().unwrap().clone()],
                ".." => { path.pop().unwrap(); }
                _ => {
                    let new_path = path.last().unwrap().lock().unwrap().dirs.get(name).unwrap().clone();
                    path.push(new_path);
                }
            },
            TerminalLine::File(size) => { path.last().unwrap().lock().unwrap().files_size += size; }
            TerminalLine::Dir(name) => { path.last().unwrap().lock().unwrap().dirs.insert(name.clone(), Default::default()); }
        }
    }

    fs
}

pub enum TerminalLine {
    ChangeDir(String),
    File(usize),
    Dir(String),
}

#[derive(Debug, Default)]
struct Dir {
    pub dirs: HashMap<String, Rc<Mutex<Self>>>,
    pub files_size: usize,

    size: Option<usize>,
}

impl Dir {
    pub fn size(&mut self) -> usize {
        if let Some(size) = self.size {
            size
        } else {
            let size = self.dirs.iter().fold(self.files_size, |acc, (_, dir)| acc + dir.lock().unwrap().size());
            self.size = Some(size);
            size
        }
    }
}

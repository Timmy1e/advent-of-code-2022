//! # Day 8: Treetop Tree House
//! https://adventofcode.com/2022/day/8

use std::sync::{Arc, Mutex};

use aoc_runner_derive::{aoc, aoc_generator};
use rayon::prelude::*;

#[aoc_generator(day8)]
pub fn prepare_input(input: &str) -> Vec<Vec<Arc<Tree>>> {
    input
        .par_lines()
        .map(|line| {
            line.par_chars()
                .map(|ch| Arc::new(Tree::new(ch.to_digit(10).unwrap() as usize)))
                .collect()
        })
        .collect()
}

/// ## Consider your map; how many trees are visible from outside the grid?
#[aoc(day8, part1)]
pub fn solve_part1(input: &[Vec<Arc<Tree>>]) -> usize {
    input
        .par_iter()
        .enumerate()
        .map(|(y, line)| {
            line.par_iter()
                .enumerate()
                .map(|(x, tree)| {
                    usize::from(
                        x == 0
                            || y == 0
                            || x == line.len() - 1
                            || y == input.len() - 1
                            || tree.is_visible(input, x, y),
                    )
                })
                .sum::<usize>()
        })
        .sum()
}

/// ## Consider each tree on your map. What is the highest scenic score possible for any tree?
#[aoc(day8, part2)]
pub fn solve_part2(input: &[Vec<Arc<Tree>>]) -> usize {
    input
        .par_iter()
        .enumerate()
        .map(|(y, line)| {
            line.par_iter()
                .enumerate()
                .map(|(x, tree)| {
                    if x == 0 || y == 0 || x == line.len() - 1 || y == input.len() - 1 {
                        0
                    } else {
                        tree.scenic_score(input, x, y)
                    }
                })
                .max()
                .unwrap()
        })
        .max()
        .unwrap()
}

#[derive(Debug)]
pub struct Tree {
    pub height: usize,
    highest_east: Mutex<Option<usize>>,
    highest_west: Mutex<Option<usize>>,
    highest_north: Mutex<Option<usize>>,
    highest_south: Mutex<Option<usize>>,
}

impl Tree {
    pub fn new(height: usize) -> Self {
        Self {
            height,
            highest_east: Default::default(),
            highest_west: Default::default(),
            highest_north: Default::default(),
            highest_south: Default::default(),
        }
    }

    pub fn is_visible(&self, forest: &[Vec<Arc<Tree>>], x: usize, y: usize) -> bool {
        self.height > self.highest_east(forest, x, y)
            || self.height > self.highest_west(forest, x, y)
            || self.height > self.highest_north(forest, x, y)
            || self.height > self.highest_south(forest, x, y)
    }

    pub fn scenic_score(&self, forest: &[Vec<Arc<Tree>>], x: usize, y: usize) -> usize {
        forest[y][x + 1].scenic_east(self.height, forest, x + 1, y)
            * forest[y][x - 1].scenic_west(self.height, forest, x - 1, y)
            * forest[y - 1][x].scenic_north(self.height, forest, x, y - 1)
            * forest[y + 1][x].scenic_south(self.height, forest, x, y + 1)
    }

    pub fn highest_east(&self, forest: &[Vec<Arc<Tree>>], x: usize, y: usize) -> usize {
        let mut guard = self.highest_east.lock().unwrap();
        if let Some(largest) = *guard {
            largest
        } else {
            let x = x + 1;
            *guard = Some(if x >= forest[y].len() {
                0
            } else {
                let tree = &forest[y][x];
                tree.height.max(tree.highest_east(forest, x, y))
            });
            guard.unwrap()
        }
    }

    fn scenic_east(&self, height: usize, forest: &[Vec<Arc<Tree>>], x: usize, y: usize) -> usize {
        if self.height >= height || x + 1 >= forest[y].len() {
            1
        } else {
            1 + forest[y][x + 1].scenic_east(height, forest, x + 1, y)
        }
    }

    pub fn highest_west(&self, forest: &[Vec<Arc<Tree>>], x: usize, y: usize) -> usize {
        let mut guard = self.highest_west.lock().unwrap();
        if let Some(largest) = *guard {
            largest
        } else {
            *guard = Some(if x == 0 {
                0
            } else {
                let x = x - 1;
                let tree = &forest[y][x];
                tree.height.max(tree.highest_west(forest, x, y))
            });
            guard.unwrap()
        }
    }

    fn scenic_west(&self, height: usize, forest: &[Vec<Arc<Tree>>], x: usize, y: usize) -> usize {
        if self.height >= height || x == 0 {
            1
        } else {
            1 + forest[y][x - 1].scenic_west(height, forest, x - 1, y)
        }
    }

    pub fn highest_north(&self, forest: &[Vec<Arc<Tree>>], x: usize, y: usize) -> usize {
        let mut guard = self.highest_north.lock().unwrap();
        if let Some(largest) = *guard {
            largest
        } else {
            *guard = Some(if y == 0 {
                0
            } else {
                let y = y - 1;
                let tree = &forest[y][x];
                tree.height.max(tree.highest_north(forest, x, y))
            });
            guard.unwrap()
        }
    }

    fn scenic_north(&self, height: usize, forest: &[Vec<Arc<Tree>>], x: usize, y: usize) -> usize {
        if self.height >= height || y == 0 {
            1
        } else {
            1 + forest[y - 1][x].scenic_north(height, forest, x, y - 1)
        }
    }

    pub fn highest_south(&self, forest: &[Vec<Arc<Tree>>], x: usize, y: usize) -> usize {
        let mut guard = self.highest_south.lock().unwrap();
        if let Some(largest) = *guard {
            largest
        } else {
            let y = y + 1;
            *guard = Some(if y >= forest.len() {
                0
            } else {
                let tree = &forest[y][x];
                tree.height.max(tree.highest_south(forest, x, y))
            });
            guard.unwrap()
        }
    }

    fn scenic_south(&self, height: usize, forest: &[Vec<Arc<Tree>>], x: usize, y: usize) -> usize {
        if self.height >= height || y + 1 >= forest.len() {
            1
        } else {
            1 + forest[y + 1][x].scenic_south(height, forest, x, y + 1)
        }
    }
}

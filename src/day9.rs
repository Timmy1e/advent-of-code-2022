//! # Day 9: Rope Bridge
//! https://adventofcode.com/2022/day/9

use std::cmp::Ordering;
use std::collections::HashSet;

use aoc_runner_derive::{aoc, aoc_generator};

#[aoc_generator(day9)]
pub fn prepare_input(input: &str) -> Vec<Command> {
    input
        .lines()
        .map(|line| {
            let (dir, steps) = line.split_once(' ').unwrap();
            (
                match dir {
                    "U" => Direction::Up,
                    "D" => Direction::Down,
                    "L" => Direction::Left,
                    "R" => Direction::Right,
                    _ => panic!("Unknown direction {:?}", dir),
                },
                steps.parse().unwrap(),
            )
        })
        .collect()
}

/// ## Simulate your complete hypothetical series of motions. How many positions does the tail of the rope visit at least once?
#[aoc(day9, part1)]
pub fn solve_part1(input: &[Command]) -> usize {
    let mut head = (0_isize, 0_isize);
    let mut tail = (0_isize, 0_isize);
    let mut tail_locations = HashSet::from([tail]);

    for (direction, steps) in input {
        for _ in 0..*steps {
            let prev_head = head;
            match direction {
                Direction::Up => head.1 -= 1,
                Direction::Down => head.1 += 1,
                Direction::Left => head.0 -= 1,
                Direction::Right => head.0 += 1,
            }
            if tail.0.abs_diff(head.0) > 1 || tail.1.abs_diff(head.1) > 1 {
                tail = prev_head;
                tail_locations.insert(tail);
            }
        }
    }

    tail_locations.len()
}

/// ## Simulate your complete series of motions on a larger rope with ten knots. How many positions does the tail of the rope visit at least once?
#[aoc(day9, part2)]
pub fn solve_part2(input: &[Command]) -> usize {
    let mut rope = [(0_isize, 0_isize); 10];
    let mut tail_locations = HashSet::from([(0_isize, 0_isize)]);

    for (direction, steps) in input {
        for _ in 0..*steps {
            let head = &mut rope[0];
            match direction {
                Direction::Up => head.1 -= 1,
                Direction::Down => head.1 += 1,
                Direction::Left => head.0 -= 1,
                Direction::Right => head.0 += 1,
            }

            for knot in 1..rope.len() {
                let prev = rope[knot - 1];
                let this = &mut rope[knot];

                if this.0.abs_diff(prev.0) <= 1 && this.1.abs_diff(prev.1) <= 1 {
                    continue;
                }

                match prev.0.cmp(&this.0) {
                    Ordering::Less => this.0 -= 1,
                    Ordering::Greater => this.0 += 1,
                    _ => {}
                }
                match prev.1.cmp(&this.1) {
                    Ordering::Less => this.1 -= 1,
                    Ordering::Greater => this.1 += 1,
                    _ => {}
                }
            }
            tail_locations.insert(*rope.last().unwrap());
        }
    }

    tail_locations.len()
}

pub type Command = (Direction, usize);

#[derive(Debug)]
pub enum Direction {
    Up,
    Down,
    Left,
    Right,
}
